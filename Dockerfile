FROM node:18-alpine as builder

WORKDIR /app
COPY . .
RUN npm install && npm run build

FROM sebp/lighttpd:latest

WORKDIR /var/www/localhost/htdocs
RUN mkdir /var/cache/compress
COPY --from=builder /app/dist /var/www/localhost/htdocs
COPY lighttpd /etc/lighttpd
