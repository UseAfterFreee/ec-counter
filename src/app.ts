import { interval } from 'rxjs';
window.onload = () => {
    let counter = document.querySelector("#ec-counter");

    let startDate = new Date();

    let iv = 10;
    interval(iv).subscribe(_ => {
        const seconds = Math.abs(new Date() - startDate) / 1000;
        let ec = (seconds * 0.00000992063).toFixed(6);

        let unit = "";
        if (ec < 1e-3) {
            unit = "μ";
            ec *= 1e6;
            ec = parseInt(ec, 10);
        } else if (ec < 1) {
            unit = "m";
            ec *= 1e3;
            ec = ec.toFixed(3);
        }

        counter.innerHTML = `${ec} ${unit}EC`;
    })
}
